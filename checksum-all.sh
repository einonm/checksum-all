#! /usr/bin/env bash
#
# checksum-all.sh: A program to recurse through a directory structure,
#                  producing checksum files for all files within. If a
#                  checksum file already exists, this is used to
#                  validate the file's checksum.
#
# Copyright (C) 2016  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
prgname=$0
exit_status=0

IFS=$(echo -en "\n\b")

usage () {
    echo "A program to sha1 checksum all files in a directory tree" >&2
    echo "Usage: $prgname [-h] [FILE PATH] " >&2
}

PARSED=`getopt 'h' "$@"`
[ $? != 0 ] && exit 2

eval set -- "$PARSED"

while [ $# -gt 0 ]
do
    case "$1" in
	-h)  usage ; exit 0 ;;
	--)  shift; break ;;
    esac
done

path=$1

# for each file (excluding hidden files.dirs)@
for FILE in `find $1 -not -path '*/\.*' -type f`
do
    # skip .sha1 and .md5 files by default
    if [[ ("$FILE" == *.sha1)  || ("$FILE" == *.md5) ]]; then
        continue
    fi

    # if there's already a .sha1 file existing...
    if [ -e "$FILE".sha1 ]; then
        # compare the checksums (reports if different)
        sha1sum --quiet -c "$FILE.sha1"
        if [ $? != 0 ]; then
            exit_status=1
        fi

    else
        # create a .sha1 checksum file, put alongside the actual file
        sha1sum "$FILE" > "$FILE.sha1"
        echo "$FILE.sha1 not found, creating"
    fi

done

exit $exit_status
